import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HumanBuilderAssistTest {
    HumanBuilderAssist humanBuilderAssist = new HumanBuilderAssist();

    @Test
    void getKilocaloriesPerDayMaleTest(){
        int res  = humanBuilderAssist.getKilocaloriesPerDay(70,180,24,Gender.MALE);
        assertEquals(1753,res);
    }

    @Test
    void getKilocaloriesPerDayFemaleTest(){
        int res  = humanBuilderAssist.getKilocaloriesPerDay(70,180,24,Gender.FEMALE);
        assertEquals(1528,res);
    }

    @Test
    void wrongHumanArgWeightDoesThrowExceptionTest(){
        assertThrows(IllegalArgumentException.class, () -> humanBuilderAssist.wrongHumanArgException(-10,180,24));
    }

    @Test
    void wrongHumanArgAgeDoesThrowExceptionTest(){
        assertThrows(IllegalArgumentException.class, () -> humanBuilderAssist.wrongHumanArgException(10,180,-24));
    }

    @Test
    void wrongHumanArgDoesNotThrowExceptionTest(){
        assertDoesNotThrow(() -> humanBuilderAssist.wrongHumanArgException(80,180,24));
    }

}
