import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HumanBuilderTest {
    HumanBuilder humanBuilder = new HumanBuilder(new HumanBuilderAssist());

    @Test
    void HumanBuilderTest(){
        Human human = new Human("John",180, 24,70,Gender.MALE,1753);
        assertEquals(human,humanBuilder.buildHuman("John",70,180,24,Gender.MALE));
    }


    @Test
    void humanNameIsNullTest(){
        assertNull(humanBuilder.buildHuman("",70,180,24,Gender.MALE));
    }

    @Test
    void humanWeightLessThanZeroTest(){
        assertThrows(IllegalArgumentException.class,() ->humanBuilder.buildHuman("John",-18,180,24,Gender.MALE));
    }

    @Test
    void humanHeightLessThanZeroTest(){
        assertThrows(IllegalArgumentException.class,() -> humanBuilder.buildHuman("John",70,-180,24,Gender.MALE));
    }

    @Test
    void humanAgeLessThanZeroTest(){
        assertThrows(IllegalArgumentException.class,() -> humanBuilder.buildHuman("John",-18,180,-24,Gender.MALE));
    }
}
