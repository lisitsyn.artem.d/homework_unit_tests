import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.gen5.api.Assertions.assertEquals;

public class HumanBuilderMockTest {
    HumanBuilderAssist humanBuilderAssist = Mockito.mock(HumanBuilderAssist.class);

    HumanBuilder humanBuilder = new HumanBuilder(humanBuilderAssist);

    @Test
    void buildHumanTest(){
        Mockito.when(humanBuilderAssist.getKilocaloriesPerDay(70,180,24,Gender.MALE)).thenReturn(1753);
        Human human = new Human("John",180,24,70,Gender.MALE,1753);

        assertEquals(human, humanBuilder.buildHuman("John",70,180,24,Gender.MALE));
    }
}
