import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.gen5.api.Assertions.assertEquals;

class HumanBuilderSpyTest {
    HumanBuilderAssist humanBuilderAssist = Mockito.spy(HumanBuilderAssist.class);

    HumanBuilder humanBuilder = new HumanBuilder(humanBuilderAssist);

    @Test
    void buildHumanTest(){
        Human human = new Human("John",180,24,70,Gender.MALE,1753);
        assertEquals(human, humanBuilder.buildHuman("John",70,180,24,Gender.MALE));
    }
}
