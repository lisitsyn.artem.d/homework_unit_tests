import java.util.Objects;

public class Human {
    private String name;
    private int height;
    private int age;
    private int weight;
    private Gender gender;
    private int kilocaloriesPerDay;

    public Human(String name, int height, int age, int weight, Gender gender,int kilocaloriesPerDay) {
        this.name = name;
        this.height = height;
        this.age = age;
        this.weight = weight;
        this.gender = gender;
        this.kilocaloriesPerDay = kilocaloriesPerDay;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Human human = (Human) o;
        return height == human.height && age == human.age && weight == human.weight && kilocaloriesPerDay == human.kilocaloriesPerDay && Objects.equals(name, human.name) && gender == human.gender;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, height, age, weight, gender, kilocaloriesPerDay);
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", height=" + height +
                ", age=" + age +
                ", weight=" + weight +
                ", gender=" + gender +
                ", kilocaloriesPerDay=" + kilocaloriesPerDay +
                '}';
    }
}
