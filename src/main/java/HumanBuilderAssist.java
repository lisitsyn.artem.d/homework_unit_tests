public class HumanBuilderAssist {
    public void wrongHumanArgException(int weight, int height, int age) throws IllegalArgumentException{
        if(weight<0){
            throw  new IllegalArgumentException("The value of human's weight can not be negative!");
        }
        else if(age<0){
            throw  new IllegalArgumentException("The value of human's age can not be negative!");
        }
        else if(height<0){
            throw  new IllegalArgumentException("The value of human's height can not be negative!");
        }
    }

    public int getKilocaloriesPerDay(int weight, int height, int age, Gender gender){
        if(gender.equals(Gender.MALE)){
            return (int) (88.3 + (13.4 * weight) + (4.8 * height)- (5.7 * age));
        }
        else{
            return (int) (447.5 + (9.2 * weight) + (3 * height)- (4.3 * age));
        }
    }
}
