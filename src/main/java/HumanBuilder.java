public class HumanBuilder {
    HumanBuilderAssist humanBuilderAssist;

    public HumanBuilder(HumanBuilderAssist humanBuilderAssist) {
        this.humanBuilderAssist = humanBuilderAssist;
    }

    public Human buildHuman(String name, int weight, int height, int age, Gender gender) {
        humanBuilderAssist.wrongHumanArgException(weight, height, age);
        if (!name.equals("")) {
            return new Human(name, height, age, weight, gender,humanBuilderAssist.getKilocaloriesPerDay(weight, height, age, gender));
        }
        else {
            return null;
        }
    }
}
